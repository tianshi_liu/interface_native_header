/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup Camera
 * @{
 *
 * @brief Provides APIs for the camera module.
 *
 * You can use the APIs to perform operations on camera devices and streams (including offline streams),
 * and implement various callbacks.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file ICameraHostCallback.idl
 *
 * @brief Declares callbacks for status changes of cameras and flashes. The caller needs to implement these callbacks.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Defines the package path of the camera module APIs.
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.camera.v1_0;

import ohos.hdi.camera.v1_0.Types;

/**
 * @brief Defines the callbacks for camera devices.
 *
 * You can implement the callbacks to obtain the device status ID, flash status, and event ID.
 */
[callback] interface ICameraHostCallback {
     /**
     * @brief Called when the camera status changes to report the latest status.
     *
     * @param cameraId Indicates the ID of the camera whose status changes.
     * @param status Indicates the latest status of the camera.
     *
     * @since 3.2
     * @version 1.0
     */
    OnCameraStatus([in] String cameraId, [in] enum CameraStatus status);

     /**
     * @brief Called when the flash status changes to report the latest status.
     *
     * @param cameraId Indicates the ID of the camera to which the flash is bound.
     * @param status Indicates the latest status of the flash. For details, see {@link CameraStatus}.
     *
     * @since 3.2
     * @version 1.0
     */
    OnFlashlightStatus([in] String cameraId, [in] enum FlashlightStatus status);

     /**
     * @brief Called when a camera event occurs.
     *
     * @param cameraId Indicates the ID of the camera to which the camera event is bound.
     * @param event Indicates the camera event. For details, see {@link CameraEvent}.
     *
     * @since 3.2
     * @version 1.0
     */
    OnCameraEvent([in] String cameraId, [in] enum CameraEvent event);
}
/** @} */
