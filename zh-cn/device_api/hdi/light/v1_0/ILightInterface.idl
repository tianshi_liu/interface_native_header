/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Light
 * @{
 *
 * @brief 灯驱动对灯服务提供通用的接口能力。
 *
 * 灯模块为灯服务提供通用的接口去访问灯驱动，服务获取灯驱动对象或代理后，可以通过调用的APIs接口获取相关的灯信息。
 * 例如打开或关闭灯、根据灯类型ID设置灯闪烁模式。
 *
 * @since 3.1
 * @version 1.0
 */

/**
 * @file ILightInterface.idl
 *
 * @brief 定义灯模块的通用接口能力，包括获取灯类型ID、打开或关闭灯光、设置灯的亮度和闪烁模式。
 *
 * @since 3.1
 * @version 1.0
 */

/**
 * @brief 灯模块接口的包路径。
 *
 * @since 3.1
 * @version 1.0
 */
package ohos.hdi.light.v1_0;
import ohos.hdi.light.v1_0.LightTypes;

/**
 * @brief 提供灯模块基本操作接口。
 *
 * 操作包括获取灯的信息、打开或关闭灯、设置灯的亮度或闪烁模式。
 *
 * @since 3.1
 * @version 1.0
 */
interface ILightInterface {
    /**
     * @brief 获取当前系统中所有类型的灯信息。
     *
     * @param info 表示指向灯信息的二级指针。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.1
     * @version 1.0
     */
    GetLightInfo([out] struct HdfLightInfo[] info);

    /**
     * @brief 根据指定的灯类型ID打开列表中的可用灯。
     *
     * @param lightId 表示灯类型ID。详见{@link HdfLightId}。
     * @param effect 表示指向灯效果的指针，如果lightbrightness字段为0时，
     * 灯的亮度根据HCS配置的默认亮度进行设置。详见{@link HdfLightEffect}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果不支持灯类型ID，则返回-1。
     * @return 如果不支持闪烁设置，则返回-2。
     * @return 如果不支持亮度设置，则返回-3。
     *
     * @since 3.1
     * @version 1.0
     */
    TurnOnLight([in] int lightId, [in] struct HdfLightEffect effect);

     /**
     * @brief 根据指定的灯类型ID打开相应灯中包含的多个子灯。
     *
     * @param lightId 表示灯类型ID，详见{@link HdfLightId}。
     * @param colors 多个子灯对应的颜色和亮度, 详见{@link HdfLightColor}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    TurnOnMultiLights([in] int lightId, [in] struct HdfLightColor[] colors);

    /**
     * @brief 根据指定的灯类型ID关闭列表中的可用灯。
     *
     * @param lightId 表示灯类型ID，详见{@link HdfLightId}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.1
     * @version 1.0
     */
    TurnOffLight([in] int lightId);
}
/** @} */
